const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const bcrypt = require('bcrypt');

async function main() {
  const users = await prisma.user.createMany(
    {
      data: [
        {
          nama: "admin",
          email: "admin@gmail.com",
          telepon: '08123456780',
          role: 'admin',
          password: await bcrypt.hash('secretpass', 10),
          status: 'active'
        },
        {
          nama: "Aldi",
          email: "aldi@gmail.com",
          telepon: '08123456789',
          role: 'user',
          password: await bcrypt.hash('secretpass', 10),
          status: 'active'
        },
      ]
    }
  );
  const products = await prisma.product.createMany(
    {
      data: [
        {
          nama: "Sepatu",
          harga: '545.000',
          gambar: 'sepatu.jpg',
          status: 'active'
        },
        {
          nama: "Mouse",
          harga: '79.000',
          gambar: 'mouse.jpg',
          status: 'active'
        },
        {
          nama: "Tas",
          harga: '245.000',
          gambar: 'tas.jpg',
          status: 'active'
        },
        {
          nama: "Keyboard",
          harga: '197.000',
          gambar: 'keyboard.jpg',
          status: 'active'
        },
      ]
    }
  );

  console.log(users, products);
}

main().then(async () => {
  await prisma.$disconnect()
}).catch(async (e) => {
  console.error(e)
  await prisma.$disconnect()
  process.exit(1)
})